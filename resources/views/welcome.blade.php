<!DOCTYPE html>
<html>

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="shortcut icon" href="images/favicon.png" type="">

  <title> PakarStunting </title>

  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

  <!-- fonts style -->
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">

  <!--owl slider stylesheet -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">

  <!-- font awesome style -->
  <link href="css/font-awesome.min.css" rel="stylesheet" />

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet" />
  <!-- responsive style -->
  <link href="css/responsive.css" rel="stylesheet" />

</head>

<body>

  <div class="hero_area">

    <!-- <div class="hero_bg_box">
      <img src="images/hero-bg.png" alt="">
    </div> -->

    <!-- header section strats -->
    <header class="header_section w-100" style="background-color: #1fab89;position:fixed;z-index: 2;">
      <div class="container">
        <nav class="navbar navbar-expand-lg custom_nav-container ">
          <a class="navbar-brand" href="">
            <img width="50" src="images/LOGO1.png" alt="" srcset="">
            <span>
              Pakar Stunting
            </span>
          </a>

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class=""> </span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
              <!-- <li class="nav-item active">
                <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#tips"> Tips</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#faq"> FAQ</a>
              </li> -->
              @if(auth('parents')->check())
              <li class="nav-item px-2">
                  <a class="btn btn-sm bg-white rounded-pill order-1 order-lg-0" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                      @csrf
                  </form>
              </li>
              @else
              <li class="nav-item">
                <a href="/login" class="btn btn-sm bg-white rounded-pill">
                  Login
                </a>
              </li>
              @endif
            </ul>
          </div>
          
        </nav>
      </div>
    </header>
    <!-- end header section -->
    <!-- slider section -->
    <section class="contact_section layout_padding" style="background-color: #1fab89;position:sticky">
      <div class="container ">
        <div class="row">
          <div class="col-md-6">
            <div class="detail-box mt-5">
              <h1 style="color: white" class="heading_container">
                Sistem Pendukung Keputusan Dalam Menentukan Balita Stunting
              </h1>
              <div class="btn-box">
                @if(auth('parents')->check())
                  <!-- <a href="#cek" class="btn1">
                    Cek Sekarang
                  </a> -->
                @else
                  <!-- <a href="/login" class="btn1">
                    Login
                  </a> -->
                @endif
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <img width="100" class="w-100" src="images/LOGO2.png" alt="">
          </div>
        </div>
      </div>

    </section>

  @if(auth('parents')->check())
  <section class="contact_section layout_padding" id="cek">
    <div class="container">
      <div class="heading_container">
        <h2>
          Hasil Pemeriksaan
        </h2>
      </div>
      <div class="row">
        <div class="col-md-12 mt-5">
            @if($asn)
            <table class="table">
                <thead>
                    <tr>
                        <th>Nama Anak</th>
                        <th>Nilai Gizi</th>
                        <th>Status Gizi</th>
                        <th>Tanggal Pemeriksaan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($asn as $item)
                        <tr>
                            <td>{{ $item->alternatif->nama }}</td>
                            <td>{{ $item->nilai }}</td>
                            <td>{{ $item->status }}</td>
                            <td>{{ $item->created_at }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @else
                @if($mode == 1)
                  Data Tidak Ditemukan
                @endif
            @endif
        </div>
      </div>
    </div>
  </section>
  @endif

  <section class="department_section layout_padding" id="info">
    <div class="department_container">
      <div class="container ">
        <div class="heading_container heading_center">
          <h2>
            Info Stunting
          </h2>
          <div class="row photos">
              <div class="col-md-4 item mt-5"><a href="/images/info/CIRI ANAK STUNTING.jpg" data-lightbox="photos"><img class="img-fluid" src="/images/info/CIRI ANAK STUNTING.jpg"></a></div>
              <div class="col-md-4 item mt-5"><a href="/images/info/Dampak Stunting.png" data-lightbox="photos"><img class="img-fluid" src="/images/info/Dampak Stunting.png"></a></div>
              <div class="col-md-4 item mt-5"><a href="/images/info/jangka panjang.png" data-lightbox="photos"><img class="img-fluid" src="/images/info/jangka panjang.png"></a></div>
              <div class="col-md-4 item mt-5"><a href="/images/info/OIP.jpg" data-lightbox="photos"><img class="img-fluid" src="/images/info/OIP.jpg"></a></div>
              <div class="col-md-4 item mt-5"><a href="/images/info/OIP (1).jpg" data-lightbox="photos"><img class="img-fluid" src="/images/info/OIP (1).jpg"></a></div>
              <div class="col-md-4 item mt-5"><a href="/images/info/OIP (2).jpg" data-lightbox="photos"><img class="img-fluid" src="/images/info/OIP (2).jpg"></a></div>
              <div class="col-md-4 item mt-5"><a href="/images/info/KENALI PENYEBABNYA.png" data-lightbox="photos"><img class="img-fluid" src="/images/info/KENALI PENYEBABNYA.png"></a></div>
              <div class="col-md-4 item mt-5"><a href="/images/info/Pencegan stunting.png" data-lightbox="photos"><img class="img-fluid" src="/images/info/Pencegan stunting.png"></a></div>
              <div class="col-md-4 item mt-5"><a href="/images/info/Stunting.png" data-lightbox="photos"><img class="img-fluid" src="/images/info/Stunting.png"></a></div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <a href="https://api.whatsapp.com/send?phone=6289527918599" target="_blank">
      <button class="btn-floating whatsapp">
          <!-- <img src="wa.png" alt="whatsApp"> -->
          <i class="fa fa-whatsapp my-float"></i>
          <span>Hubungi kami</span>
      </button>
  </a>

  <!-- footer section -->
  <footer class="footer_section">
    <div class="container">
      <div class="footer-info">
        <p>
          &copy; <span id="displayYear"></span> INFORMATIKA|Mega Purnama Sari
          <a href="https://html.design/">
        </p>
       
      </div>  
    </div>
  </footer>
  <!-- footer section -->

  <!-- jQery -->
  <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
  <!-- popper js -->
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <!-- bootstrap js -->
  <script type="text/javascript" src="js/bootstrap.js"></script>
  <!-- owl slider -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js">
  </script>
  <!-- custom js -->
  <script type="text/javascript" src="js/custom.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>

</body>

</html>