<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('antropometri_tb_u_s', function (Blueprint $table) {
            $table->id();
            $table->text('kelamin')->nullable();
            $table->integer('umur')->nullable();
            $table->decimal('min_3_sd')->nullable();
            $table->decimal('min_2_sd')->nullable();
            $table->decimal('min_1_sd')->nullable();
            $table->decimal('median')->nullable();
            $table->decimal('plus_3_sd')->nullable();
            $table->decimal('plus_2_sd')->nullable();
            $table->decimal('plus_1_sd')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('antropometri_tb_u_s');
    }
};
