<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('antropometri_bb_tbs', function (Blueprint $table) {
            $table->id();
            $table->text('kelamin')->nullable();
            $table->integer('min_umur')->nullable();
            $table->integer('max_umur')->nullable();
            $table->decimal('tinggi_badan')->nullable();
            $table->decimal('min_3_sd')->nullable();
            $table->decimal('min_2_sd')->nullable();
            $table->decimal('min_1_sd')->nullable();
            $table->decimal('median')->nullable();
            $table->decimal('plus_3_sd')->nullable();
            $table->decimal('plus_2_sd')->nullable();
            $table->decimal('plus_1_sd')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('antropometri_bb_tbs');
    }
};
