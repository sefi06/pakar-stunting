<?php

return [

    'title' => 'Input :label',

    'breadcrumb' => 'Buat',

    'form' => [

        'actions' => [

            'cancel' => [
                'label' => 'Batal',
            ],

            'create' => [
                'label' => 'Buat',
            ],

            'create_another' => [
                'label' => 'Buat & buat lainnya',
            ],

        ],

    ],

    'messages' => [
        'created' => 'Data berhasil dibuat',
    ],

];
