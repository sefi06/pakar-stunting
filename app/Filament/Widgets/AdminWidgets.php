<?php

namespace App\Filament\Widgets;

use App\Models\Alternatif;
use App\Models\Kriteria;
use App\Models\SubKriteria;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Card;

class AdminWidgets extends BaseWidget
{
    protected function getCards(): array
    {
        return [
            Card::make('Total Kriteria', Kriteria::count()),
            Card::make('Total Sub Kriteria', SubKriteria::count()),
            Card::make('Total Alternatif', Alternatif::count()),
        ];
    }
}
