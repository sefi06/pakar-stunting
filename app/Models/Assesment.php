<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assesment extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function alternatif() {
        return $this->hasOne(Alternatif::class, 'id', 'alternatif_id');
    }

    public function assesments() {
        return $this->hasMany(Assesment::class, 'alternatif_id', 'alternatif_id')->orderBy('created_at', 'desc');
    }
}
