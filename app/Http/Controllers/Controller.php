<?php

namespace App\Http\Controllers;

use App\Models\Alternatif;
use App\Models\Assesment;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(Request $request) {
        $asn = null;
        $mode= 0;

        $nik    = @auth('parents')->user()->nik;
        $alt    = Alternatif::where('nik', $nik)->first();
        
        if($alt) {
            $alt_id = $alt->id;

            $asn    = Assesment::where('alternatif_id', $alt_id)->with('alternatif')->get();
        }

        return view('welcome',[
            'asn'   => $asn,
            'mode'  => $mode
        ]);
    }
}
