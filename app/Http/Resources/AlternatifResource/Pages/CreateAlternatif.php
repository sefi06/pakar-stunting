<?php

namespace App\Http\Resources\AlternatifResource\Pages;

use App\Http\Resources\AlternatifResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateAlternatif extends CreateRecord
{
    protected static string $resource = AlternatifResource::class;
}
