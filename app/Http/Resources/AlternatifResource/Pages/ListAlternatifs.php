<?php

namespace App\Http\Resources\AlternatifResource\Pages;

use App\Http\Resources\AlternatifResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListAlternatifs extends ListRecords
{
    protected static string $resource = AlternatifResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
