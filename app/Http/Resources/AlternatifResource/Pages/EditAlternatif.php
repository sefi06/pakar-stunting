<?php

namespace App\Http\Resources\AlternatifResource\Pages;

use App\Http\Resources\AlternatifResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditAlternatif extends EditRecord
{
    protected static string $resource = AlternatifResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
