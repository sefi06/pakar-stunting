<?php

namespace App\Http\Resources;

use App\Http\Resources\AssesmentResource\Pages;
use App\Http\Resources\AssesmentResource\RelationManagers;
use App\Models\Alternatif;
use App\Models\Assesment;
use App\Models\Kriteria;
use App\Models\SubKriteria;
use DateTime;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Forms\Set;
use Filament\Resources\Resource;
use Filament\Tables\Table;
use Filament\Tables;
use Rupadana\FilamentCustomForms\Components\InputGroup;

class AssesmentResource extends Resource
{
    protected static ?string $model = Assesment::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?string $navigationLabel = 'Data Pemeriksaan';

    protected static ?string $label = 'Hasil Gizi Balita';

    protected static ?int $navigationSort = 3;

    private static function formBuilder() {
        $field[]    = Forms\Components\Select::make('alternatif_id')
                    ->required()
                    ->searchable()
                    ->label('Nama Anak')
                    ->options(Alternatif::all()->pluck('nama', 'id'));

        foreach (Kriteria::all()->pluck('nama', 'id') as $key => $value) {
            $field[]    = Forms\Components\Select::make($key)
                        ->required()
                        ->label($value)
                        ->options(SubKriteria::where('kriteria_id', $key)->get()->pluck('nama', 'id'));
        }

        return $field;
    }

    private function getUmur($ttl) {
        $dateOfBirth = new DateTime($ttl);
        $currentDate = new DateTime();

        $interval = $currentDate->diff($dateOfBirth);

        $ageInMonths = ($interval->y * 12) + $interval->m;

        return $ageInMonths; 
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Select::make('alternatif_id')
                ->required()
                ->searchable()
                ->label('Nama Anak')
                ->options(Alternatif::all()->mapWithKeys(function ($alternatif) {
                    return [$alternatif->id => $alternatif->nik . ' / ' . $alternatif->nama];
                })->toArray())
                ->live()
                ->afterStateUpdated(function (Set $set, $state) {
                    $data   = Alternatif::find($state);
                    $ttl    = $data->ttl;

                    $dateOfBirth= new DateTime($ttl);
                    $currentDate= new DateTime();

                    $interval   = $currentDate->diff($dateOfBirth);

                    $ageInMonths= ($interval->y * 12) + $interval->m;
                    $umur       = $ageInMonths;
                    $set('umur', $umur);
                }),
                Forms\Components\TextInput::make('umur')
                    ->required()
                    ->integer()
                    ->label('Umur (Bulan)')
                    ->suffix('Bulan'),
                Forms\Components\TextInput::make('bb')
                    ->required()
                    ->numeric()
                    ->label('Berat Badan (Kg)')
                    ->suffix('Kg'),
                Forms\Components\TextInput::make('tb')
                    ->required()
                    ->numeric()
                    ->label('Tinggi Badan (Cm)')
                    ->suffix('Cm'),
            ]);
            
        // return $form->schema(self::formBuilder());
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('alternatif.nik')->searchable()->label('NIK'),
                Tables\Columns\TextColumn::make('alternatif.nama')->searchable()->label('Nama Anak'),
                Tables\Columns\TextColumn::make('nilai')->label('Nilai Gizi'),
                Tables\Columns\TextColumn::make('status')->label('Status Gizi'),
                Tables\Columns\TextColumn::make('created_at')->label('Tanggal Pemeriksaan Terakhir')
                    ->dateTime(),
            ])
            ->filters([
                //
            ])
            ->actions([
                // Tables\Actions\DeleteAction::make(),
                Tables\Actions\ViewAction::make(),
            ])
            ->bulkActions([

            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            RelationManagers\AssesmentsRelationManager::class
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAssesments::route('/'),
            'create' => Pages\CreateAssesment::route('/create'),
            'edit' => Pages\EditAssesment::route('/{record}/edit'),
            'view' => Pages\EditAssesment::route('/{record}/edit'),
        ];
    }    
}
