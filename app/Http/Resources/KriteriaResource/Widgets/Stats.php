<?php

namespace App\Http\Resources\KriteriaResource\Widgets;

use Filament\Widgets\Widget;

class Stats extends Widget
{
    protected static string $view = 'http.resources.kriteria-resource.widgets.stats';
}
