<?php

namespace App\Http\Resources\KriteriaResource\RelationManagers;

use App\Models\Kriteria;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class SubKriteriasRelationManager extends RelationManager
{
    protected static string $relationship = 'sub_kriterias';

    protected static ?string $recordTitleAttribute = 'nama';

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Select::make('kriteria_id')
                    ->required()
                    ->searchable()
                    ->label('Nama Kriteria')
                    ->options(Kriteria::all()->pluck('nama', 'id')),
                Forms\Components\TextInput::make('nama')
                    ->required()
                    ->maxLength(255)
                    ->label('Keterangan'),
                Forms\Components\TextInput::make('bobot')
                    ->numeric()
                    ->required()
                    ->label('Nilai Bobot'),
            ]);
    }

    public function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('kriteria.nama')->searchable()->label('Nama Kriteria'),
                Tables\Columns\TextColumn::make('nama')->searchable()->label('Keterangan'),
                Tables\Columns\TextColumn::make('bobot')->label('Nilai Bobot')
            ])
            ->headerActions([
                Tables\Actions\CreateAction::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([

            ]);
    }    
}
