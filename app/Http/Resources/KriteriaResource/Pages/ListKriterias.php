<?php

namespace App\Http\Resources\KriteriaResource\Pages;

use App\Http\Resources\KriteriaResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListKriterias extends ListRecords
{
    protected static string $resource = KriteriaResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
