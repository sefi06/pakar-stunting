<?php

namespace App\Http\Resources\KriteriaResource\Pages;

use App\Http\Resources\KriteriaResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditKriteria extends EditRecord
{
    protected static string $resource = KriteriaResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
