<?php

namespace App\Http\Resources\KriteriaResource\Pages;

use App\Http\Resources\KriteriaResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateKriteria extends CreateRecord
{
    protected static string $resource = KriteriaResource::class;
}
