<?php

namespace App\Http\Resources;

use App\Http\Resources\KriteriaResource\Pages;
use App\Http\Resources\KriteriaResource\RelationManagers;
use App\Http\Resources\KriteriaResource\Widgets\Stats;
use App\Models\Kriteria;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Resources\Resource;
use Filament\Tables\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class KriteriaResource extends Resource
{
    protected static ?string $model = Kriteria::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?string $navigationLabel = 'Kriteria Anak';

    protected static ?string $label = 'Kriteria Anak';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('nama')
                    ->required()
                    ->maxLength(255)
                    ->label('Nama Kriteria'),
                Forms\Components\TextInput::make('atribut')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('bobot')
                    ->numeric()
                    ->required()
                    ->label('Bobot Kriteria'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('nama')->searchable()->label('Nama Kriteria'),
                Tables\Columns\TextColumn::make('atribut')->searchable(),
                Tables\Columns\TextColumn::make('bobot')->searchable()->label('Bobot Kriteria'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                // Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            RelationManagers\SubKriteriasRelationManager::class
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListKriterias::route('/'),
            'create' => Pages\CreateKriteria::route('/create'),
            'edit' => Pages\EditKriteria::route('/{record}/edit'),
        ];
    }
    
    public static function getWidgets(): array
    {
        return [
            // other widgets...
            Stats::class,
        ];
    }

    public static function getHeaderWidgets(): array
    {
        return [
            // other widgets...
            Stats::class,
        ];
    }
}
