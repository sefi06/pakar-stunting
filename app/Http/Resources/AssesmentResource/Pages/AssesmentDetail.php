<?php

namespace App\Http\Resources\AssesmentResource\Pages;

use App\Http\Resources\AssesmentResource;
use Filament\Resources\Pages\Page;
use Filament\Resources\Pages\Concerns\InteractsWithRecord;

class AssesmentDetail extends Page
{
    use InteractsWithRecord;
    
    protected static string $resource = AssesmentResource::class;

    protected static string $view = 'http.resources.assesment-resource.pages.assesment-detail';

    
    public function mount(int | string $record): void
    {
        
        $this->record = $this->resolveRecord($record);
    }
}
