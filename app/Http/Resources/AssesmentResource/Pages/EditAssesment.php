<?php

namespace App\Http\Resources\AssesmentResource\Pages;

use App\Http\Resources\AssesmentResource;
use App\Models\Assesment;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Support\Htmlable;

class EditAssesment extends EditRecord
{
    protected static string $resource = AssesmentResource::class;

    protected static string $view = 'http.resources.assesment-resource.pages.assesment-detail';

    protected function getHeaderActions(): array
    {
        return [
            // Actions\DeleteAction::make(),
        ];
    }

    public function getTitle(): string | Htmlable
    {
        return 'Riwayat  ' . $this->getRecordTitle();
    }
}
