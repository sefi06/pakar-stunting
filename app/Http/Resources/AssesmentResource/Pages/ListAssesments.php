<?php

namespace App\Http\Resources\AssesmentResource\Pages;

use App\Http\Resources\AssesmentResource;
use App\Models\Assesment;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class ListAssesments extends ListRecords
{
    protected static string $resource = AssesmentResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }

    protected function getTableQuery(): Builder
    {
        return Assesment::query()
            ->select('assesments.*')
            ->join(
                DB::raw("(SELECT alternatif_id, MAX(created_at) AS max_created_at FROM assesments GROUP BY alternatif_id) as t2"),
                function ($join) {
                    $join->on('assesments.alternatif_id', '=', 't2.alternatif_id')
                        ->whereColumn('assesments.created_at', '=', 't2.max_created_at');
                }
            );
    }

}
