<?php

namespace App\Http\Resources\AssesmentResource\Pages;

use App\Http\Resources\AssesmentResource;
use App\Models\Alternatif;
use App\Models\AntropometriBbTb;
use App\Models\AntropometriBbU;
use App\Models\AntropometriImtU;
use App\Models\AntropometriTbU;
use App\Models\Assesment;
use App\Models\AssesmentDetail;
use App\Models\Kriteria;
use App\Models\SubKriteria;
use Filament\Notifications\Notification;
use Filament\Resources\Pages\CreateRecord;

class CreateAssesment extends CreateRecord
{
    protected static string $resource = AssesmentResource::class;

    protected function mutateFormDataBeforeCreate(array $data): array
    {
        $param['alternatif_id'] = $data['alternatif_id'];

        unset($data['alternatif_id']);

        $param['sampel']        = json_encode($data);

        return $param;
    }

    protected function getRedirectUrl(): string
    {
        return $this->previousUrl ?? $this->getResource()::getUrl('index');
    }

    private function getStatus($value) {
        if ($value >= 0 && $value <= 0.25) {
            return "Sangat pendek";
        } elseif ($value > 0.25 && $value <= 0.50) {
            return "Stunting";
        } elseif ($value > 0.50 && $value <= 0.75) {
            return "Tinggi";
        } elseif ($value > 0.75 && $value <= 1.00) {
            return "Normal";
        } else {
            return "Invalid value";
        }
    }

    private function getScroreBbU($score, $antropometri) {
        if ($score < $antropometri->min_3_sd) {
            $status = 'Berat badan sangat kurang';
        } elseif ($score >= $antropometri->min_3_sd && $score < $antropometri->min_2_sd) {
            $status = 'Berat badan kurang';
        } elseif ($score >= $antropometri->min_2_sd && $score <= $antropometri->plus_1_sd) {
            $status = 'Berat badan normal';
        } elseif ($score > $antropometri->plus_1_sd) {
            $status = 'Risiko Berat badan lebih';
        } else {
            $status = 'Berat badan sangat kurang';
        }

        $kriteria       = Kriteria::where('nama', 'BB/U')->first();
        $subkriteria    = SubKriteria::where('kriteria_id', $kriteria->id)
                            ->where(function ($query) use ($status) {
                                $query->whereRaw('LOWER(nama) = ?', [strtolower($status)])
                                    ->orWhereRaw('LOWER(nama) LIKE ?', ['%' . strtolower($status) . '%']);
                            })
                            ->first();

        if(!$subkriteria) {
            Notification::make()
                ->warning()
                ->title('kriteria / Subkriteria BB/U tidak ditemukan')
                ->send();
        
            $this->halt();
        }

        return  [
            $kriteria->id   => $subkriteria->id
        ];

    }

    private function getScroreTbU($score, $antropometri) {
        if ($score < $antropometri->min_3_sd) {
            $status = 'Sangat pendek';
        } elseif ($score >= $antropometri->min_3_sd && $score < $antropometri->min_2_sd) {
            $status = 'Pendek';
        } elseif ($score >= $antropometri->min_2_sd && $score <= $antropometri->plus_3_sd) {
            $status = 'Normal';
        } elseif ($score > $antropometri->plus_3_sd) {
            $status = 'Tinggi';
        } else {
            $status = 'Sangat pendek';
        }

        $kriteria       = Kriteria::where('nama', 'TB/U')->first();
        $subkriteria    = SubKriteria::where('kriteria_id', $kriteria->id)
                            ->where(function ($query) use ($status) {
                                $query->whereRaw('LOWER(nama) = ?', [strtolower($status)])
                                    ->orWhereRaw('LOWER(nama) LIKE ?', ['%' . strtolower($status) . '%']);
                            })
                            ->first();

        if(!$subkriteria) {
            Notification::make()
                ->warning()
                ->title('kriteria / Subkriteria TB/U tidak ditemukan')
                ->send();
        
            $this->halt();
        }

        return  [
            $kriteria->id   => $subkriteria->id
        ];

    }

    private function getScroreBbTb($score, $antropometri) {
        if ($score < $antropometri->min_3_sd) {
            $status = 'Gizi buruk';
        } elseif ($score >= $antropometri->min_3_sd && $score < $antropometri->min_2_sd) {
            $status = 'Gizi kurang';
        } elseif ($score >= $antropometri->min_2_sd && $score < $antropometri->plus_1_sd) {
            $status = 'Gizi baik';
        } elseif ($score >= $antropometri->plus_1_sd && $score < $antropometri->plus_2_sd) {
            $status = 'Berisiko gizi lebih';
        } elseif ($score >= $antropometri->plus_2_sd && $score <= $antropometri->plus_3_sd) {
            $status = 'Gizi lebih';
        } elseif ($score > $antropometri->plus_3_sd) {
            $status = 'Obesitas';
        } else {
            $status = 'Gizi buruk';
        }

        $kriteria       = Kriteria::where('nama', 'BB/TB')->first();
        $subkriteria    = SubKriteria::where('kriteria_id', $kriteria->id)
                            ->where(function ($query) use ($status) {
                                $query->whereRaw('LOWER(nama) = ?', [strtolower($status)])
                                    ->orWhereRaw('LOWER(nama) LIKE ?', ['%' . strtolower($status) . '%']);
                            })
                            ->first();
        
        if(!$subkriteria) {
            Notification::make()
                ->warning()
                ->title('kriteria / Subkriteria BB/TB tidak ditemukan')
                ->send();
        
            $this->halt();
        }

        return  [
            $kriteria->id   => $subkriteria->id
        ];

    }

    private function getScroreImtU($score, $antropometri) {
        if ($score < $antropometri->min_3_sd) {
            $status = 'Gizi buruk';
        } elseif ($score >= $antropometri->min_3_sd && $score < $antropometri->min_2_sd) {
            $status = 'Gizi kurang';
        } elseif ($score >= $antropometri->min_2_sd && $score < $antropometri->plus_1_sd) {
            $status = 'Gizi baik';
        } elseif ($score >= $antropometri->plus_1_sd && $score < $antropometri->plus_2_sd) {
            $status = 'Berisiko gizi lebih';
        } elseif ($score >= $antropometri->plus_2_sd && $score <= $antropometri->plus_3_sd) {
            $status = 'Gizi lebih';
        } elseif ($score > $antropometri->plus_3_sd) {
            $status = 'Obesitas';
        } else {
            $status = 'Gizi buruk';
        }

        $kriteria       = Kriteria::where('nama', 'IMT/U')->first();
        $subkriteria    = SubKriteria::where('kriteria_id', $kriteria->id)
                            ->where(function ($query) use ($status) {
                                $query->whereRaw('LOWER(nama) = ?', [strtolower($status)])
                                    ->orWhereRaw('LOWER(nama) LIKE ?', ['%' . strtolower($status) . '%']);
                            })
                            ->first();
        
        if(!$subkriteria) {
            Notification::make()
                ->warning()
                ->title('kriteria / Subkriteria IMT/U tidak ditemukan')
                ->send();
        
            $this->halt();
        }

        return  [
            $kriteria->id   => $subkriteria->id
        ];

    }

    protected function afterCreate(): void
    {
        $dataform   = $this->data;

        $param['alternatif_id'] = $dataform['alternatif_id'];

        unset($dataform['alternatif_id']);

        $dataBalita         = Alternatif::find($param['alternatif_id']);
        $jenis_kelamin      = $dataBalita->jenis_kelamin;


        //perhitungan BB/U
        $antropopmetriBbU   = AntropometriBbU::where('umur', $dataform['umur'])->where('kelamin', $jenis_kelamin)->first();
            
        if ($dataform['bb'] > $antropopmetriBbU->median) {
            $r1 = $dataform['bb'] - $antropopmetriBbU->median;
            $r2 = $antropopmetriBbU->plus_1_sd - $antropopmetriBbU->median;
            $r3 = $r1 / $r2;
        }
        else {
            $r1 = $dataform['bb'] - $antropopmetriBbU->median;
            $r2 = $antropopmetriBbU->median - $antropopmetriBbU->min_1_sd;
            $r3 = $r1 / $r2;
        }

        $data_bb_u  = $this->getScroreBbU(round($r3), $antropopmetriBbU);


        //perhitungan TB/U
        $antropopmetriTbU   = AntropometriTbU::where('umur', $dataform['umur'])->where('kelamin', $jenis_kelamin)->first();
        
        if ($dataform['tb'] > $antropopmetriTbU->median) {
            $r1 = $dataform['tb'] - $antropopmetriTbU->median;
            $r2 = $antropopmetriTbU->plus_1_sd - $antropopmetriTbU->median;
            $r3 = $r1 / $r2;
        }
        else {
            $r1 = $dataform['tb'] - $antropopmetriTbU->median;
            $r2 = $antropopmetriTbU->median - $antropopmetriTbU->min_1_sd;
            $r3 = $r1 / $r2;
        }

        $data_tb_u  = $this->getScroreTbU(round($r3), $antropopmetriTbU);


        //perhitungan BB/TB
        $antropometriBbTb   = AntropometriBbTb::where('kelamin', $jenis_kelamin)
                                ->where('tinggi_badan', $dataform['tb'])
                                ->where('min_umur', '<=', $dataform['umur'])
                                ->where('max_umur', '>=', $dataform['umur'])
                                ->first()
                                ;

        if ($dataform['bb'] > $antropometriBbTb->median) {
            $r1 = $dataform['bb'] - $antropometriBbTb->median;
            $r2 = $antropometriBbTb->plus_1_sd - $antropometriBbTb->median;
            $r3 = $r1 / $r2;
        }
        else {
            $r1 = $dataform['bb'] - $antropometriBbTb->median;
            $r2 = $antropometriBbTb->median - $antropometriBbTb->min_1_sd;
            $r3 = $r1 / $r2;
        }

        $data_bb_tb  = $this->getScroreBbTb(round($r3), $antropometriBbTb);


        //perhitungan IMT/U
        $antropopmetriImtU   = AntropometriImtU::where('umur', $dataform['umur'])->where('kelamin', $jenis_kelamin)->first();
        
        $imt    = $dataform['bb'] / ($dataform['tb'] / 10000);

        if ($dataform['tb'] > $antropopmetriImtU->median) {
            $r1 = $imt - $antropopmetriImtU->median;
            $r2 = $antropopmetriImtU->plus_1_sd - $antropopmetriImtU->median;
            $r3 = $r1 / $r2;
        }
        else {
            $r1 = $imt - $antropopmetriImtU->median;
            $r2 = $antropopmetriImtU->median - $antropopmetriImtU->min_1_sd;
            $r3 = $r1 / $r2;
        }

        $data_imt_u  = $this->getScroreImtU(round($r3), $antropopmetriImtU);

        $sampel = $data_bb_u + $data_tb_u + $data_bb_tb + $data_imt_u;

        $kriteria       = Kriteria::all()->pluck('bobot', 'id');
        $subkriteria    = SubKriteria::all()->pluck('bobot', 'id');
        $assesment_id   = $this->record->getKey();

        $n = 0;

        foreach ($sampel as $key => $value) {
            $n++;
            $detail = new AssesmentDetail();
            $detail->assesment_id   = $assesment_id;
            $detail->kriteria_id    = $key;
            $detail->sub_kriteria_id= $value;
            if($n == 1 || $n == 2) {
                // BB/U & TB/U
                $detail->nilai          = $subkriteria[$value] / 1;
            }
            else {
                // BB/TB & IMT/U
                $detail->nilai          = $subkriteria[$value] / 1.5;
            }
            $detail->tipe           = 1;
            $detail->save();
        }

        $getnormalisasi = AssesmentDetail::where('assesment_id', $assesment_id)->where('tipe', 1)->get()->toArray();

        foreach ($getnormalisasi as $key => $value) {
            $detail = new AssesmentDetail();
            $detail->assesment_id   = $value['assesment_id'];
            $detail->kriteria_id    = $value['kriteria_id'];
            $detail->sub_kriteria_id= $value['sub_kriteria_id'];
            $detail->nilai          = $value['nilai'] * $kriteria[$value['kriteria_id']];
            $detail->tipe           = 2;
            $detail->save();
        }

        $getresult  = AssesmentDetail::where('assesment_id', $assesment_id)->where('tipe', 2)->get()->toArray();

        $val  = [];
        foreach ($getresult as $key => $value) {
            $val[]  = $value['nilai'];
        }

        $nilai  = array_sum($val);
        $status = $this->getStatus($nilai);

        $assesment  = Assesment::where('id', $assesment_id)->first();
        $assesment->nilai   = $nilai;
        $assesment->status  = $status;
        $assesment->save();
    }
}
