<?php

namespace App\Http\Resources;

use App\Http\Resources\AlternatifResource\Pages;
use App\Http\Resources\AlternatifResource\RelationManagers;
use App\Models\Alternatif;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Validation\Rule;

class AlternatifResource extends Resource
{
    protected static ?string $model = Alternatif::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?string $navigationLabel = 'Data Anak';

    protected static ?string $label = 'Data Anak';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('nik')
                    ->required()
                    ->unique(ignorable: fn ($record) => $record)
                    ->integer()
                    ->maxLength(16),
                Forms\Components\TextInput::make('nama')
                    ->required()
                    ->maxLength(255)
                    ->label('Nama Anak'),
                Forms\Components\Select::make('jenis_kelamin')
                    ->required()
                    ->options(['Laki - Laki' => 'Laki - Laki', 'Perempuan' => 'Perempuan']),
                Forms\Components\DatePicker::make('ttl')
                    ->required()
                    ->label('Tanggal Lahir'),
                Forms\Components\TextInput::make('orang_tua')
                    ->required()
                    ->maxLength(255)
                    ->label('Nama Orang Tua'),
                Forms\Components\Textarea::make('alamat')
                    ->required()
                    ->maxLength(65535),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('nik')->searchable(),
                Tables\Columns\TextColumn::make('nama')->searchable()->label('Nama Anak'),
                Tables\Columns\TextColumn::make('jenis_kelamin')->searchable(),
                Tables\Columns\TextColumn::make('ttl')
                    ->date(),
                Tables\Columns\TextColumn::make('orang_tua')->searchable(),
                Tables\Columns\TextColumn::make('alamat'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([

            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAlternatifs::route('/'),
            'create' => Pages\CreateAlternatif::route('/create'),
            'edit' => Pages\EditAlternatif::route('/{record}/edit'),
        ];
    }    
}
